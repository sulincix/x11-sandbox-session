build:
	: please run make install
install:
	mkdir -p $(DESTDIR)/usr/bin/
	mkdir -p $(DESTDIR)/usr/share/xsessions/
	install sandbox-session $(DESTDIR)/usr/bin/
	install sandbox-run $(DESTDIR)/usr/bin/
	install sandbox.desktop $(DESTDIR)/usr/share/xsessions/
